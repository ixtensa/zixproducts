<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */

namespace IXTENSA;


/**
 * Reads and writes news archives
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class ProductArchiveModel extends \Model
{

	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_product_archive';


	/**
	 * Get all child archives of the given set of IDs
	 *
	 * @param array   $arrPid  An array with the product archive PIDs
	 * @param array   &$arrArchives  Reference of an array in which the found IDs are stored
	 * @param array   $arrOptions  An optional options array
	 */
	public static function getChildrenByIds($arrPid, &$arrArchives, array $arrOptions=array())
	{
		if (!is_array($arrPid) || empty($arrPid) || !is_array($arrArchives) || empty($arrArchives))
		{
			return array();
		}

		$t = static::$strTable;

		$arrColumns = array("$t.pid IN(" . implode(',', array_map('intval', $arrPid)) . ")");

		$result = static::findBy($arrColumns, null, $arrOptions);

		$arrResult = array();

		if ($result !== null) {
			while ($result->next())
			{
				$arrResult[] = $result->id;
			}
			
			if (!empty($arrResult))
			{
				$arrArchives = array_merge($arrArchives, $arrResult);
				self::getChildrenByIds($arrResult, $arrArchives, $arrOptions);
			}
		}	
	}
	
}
