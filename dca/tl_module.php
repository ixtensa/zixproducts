<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['productlist']    = '{title_legend},name,headline,type;{config_legend},product_archives,numberOfItems,perPage,skipFirst;{template_legend:hide},product_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_module']['palettes']['productdetails']  = '{title_legend},name,headline,type;{config_legend},product_archives;{template_legend:hide},product_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
//$GLOBALS['TL_DCA']['tl_module']['palettes']['productarchive'] = '{title_legend},name,headline,type;{config_legend},product_archives,product_jumpToCurrent,product_detailsModule,perPage,product_format;{template_legend:hide},product_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
//$GLOBALS['TL_DCA']['tl_module']['palettes']['productmenu']    = '{title_legend},name,headline,type;{config_legend},product_archives,product_showQuantity,product_format,product_startDay,product_order;{redirect_legend},jumpTo;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';


/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['product_archives'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_archives'],
	'exclude'                 => true,
	//'inputType'               => 'checkbox',
	//'options_callback'        => array('tl_module_product', 'getProductArchives'),
	'inputType'				  => 'treePicker',
	'eval'                    => array
	(
		'multiple'=>true,
		'mandatory'=>true,
		'foreignTable'   => 'tl_product_archive',
        'titleField'     => 'id',
        'searchField'    => 'title',
        'managerHref'    => 'do=product&table=tl_product_archive',
        'fieldType'      => 'checkbox',
        'multiple'       => true,
        'pickerCallback' => function($row) {
            return $row['title'] . ' [' . $row['id'] . ']';
        }
	),
	'sql'                     => "blob NULL"
);

// TODO: instead of featured filter by certain tags

/*
$GLOBALS['TL_DCA']['tl_module']['fields']['product_featured'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_featured'],
	'default'                 => 'all_items',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('all_items', 'featured', 'unfeatured'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_module'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(16) NOT NULL default ''"
);
*/

$GLOBALS['TL_DCA']['tl_module']['fields']['product_jumpToCurrent'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_jumpToCurrent'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('hide_module', 'show_current', 'all_items'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_module'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(16) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_detailsModule'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_detailsModule'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_product', 'getDetailsModules'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_module'],
	'eval'                    => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_template'],
	'default'                 => 'product_latest',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_product', 'getProductTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_format'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_format'],
	'default'                 => 'product_month',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('product_day', 'product_month', 'product_year'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_module'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_order'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_order'],
	'default'                 => 'descending',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('ascending', 'descending'),
	'reference'               => &$GLOBALS['TL_LANG']['MSC'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_showQuantity'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_showQuantity'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'sql'                     => "char(1) NOT NULL default ''"
);


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class tl_module_product extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Get all product archives and return them as array
	 * @return array
	 */
	public function getProductArchives()
	{
		if (!$this->User->isAdmin && !is_array($this->User->product))
		{
			return array();
		}

		$arrArchives = array();
		$objArchives = $this->Database->execute("SELECT id, title FROM tl_product_archive ORDER BY title");

		while ($objArchives->next())
		{
			if ($this->User->hasAccess($objArchives->id, 'product'))
			{
				$arrArchives[$objArchives->id] = $objArchives->title;
			}
		}

		return $arrArchives;
	}


	/**
	 * Get all product details modules and return them as array
	 * @return array
	 */
	public function getDetailsModules()
	{
		$arrModules = array();
		$objModules = $this->Database->execute("SELECT m.id, m.name, t.name AS theme FROM tl_module m LEFT JOIN tl_theme t ON m.pid=t.id WHERE m.type='productdetails' ORDER BY t.name, m.name");

		while ($objModules->next())
		{
			$arrModules[$objModules->theme][$objModules->id] = $objModules->name . ' (ID ' . $objModules->id . ')';
		}

		return $arrModules;
	}


	/**
	 * Return all product templates as array
	 * @return array
	 */
	public function getproductTemplates()
	{
		return $this->getTemplateGroup('product_');
	}
}
