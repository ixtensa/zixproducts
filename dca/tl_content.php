<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Dynamically add the permission check and parent table
 */
if (Input::get('do') == 'product')
{
	$GLOBALS['TL_DCA']['tl_content']['config']['ptable'] = 'tl_product';
	$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = array('tl_content_product', 'checkPermission');
}

$GLOBALS['TL_DCA']['tl_content']['palettes']['productlist']    = '{title_legend},name,headline,type;{config_legend},product_archives,numberOfItems,perPage,skipFirst;{template_legend:hide},product_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';
$GLOBALS['TL_DCA']['tl_content']['palettes']['productdetails']  = '{title_legend},name,headline,type;{config_legend},product_archives;{template_legend:hide},product_template,customTpl;{image_legend:hide},imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';

/**
 * Add fields to tl_content
 */
$GLOBALS['TL_DCA']['tl_content']['fields']['product_archives'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_archives'],
	'exclude'                 => true,
	//'inputType'               => 'checkbox',
	//'options_callback'        => array('tl_content_product', 'getProductArchives'), // TODO: integrate es tree possible?
	'inputType'				  => 'treePicker',
	'eval'                    => array
	(
		'multiple'=>true,
		'mandatory'=>true,
		'foreignTable'   => 'tl_product_archive',
        'titleField'     => 'id',
        'searchField'    => 'title',
        'managerHref'    => 'do=product&table=tl_product_archive',
        'fieldType'      => 'checkbox',
        'multiple'       => true,
        'pickerCallback' => function($row) {
            return $row['title'] . ' [' . $row['id'] . ']';
        }
	),
	'sql'                     => "blob NULL"
);

// TODO: instead of featured filter by certain tags

/*
$GLOBALS['TL_DCA']['tl_content']['fields']['product_featured'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_featured'],
	'default'                 => 'all_items',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('all_items', 'featured', 'unfeatured'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_content'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(16) NOT NULL default ''"
);
*/

$GLOBALS['TL_DCA']['tl_content']['fields']['product_jumpToCurrent'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_jumpToCurrent'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('hide_module', 'show_current', 'all_items'), // TODO: check function!
	'reference'               => &$GLOBALS['TL_LANG']['tl_content'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(16) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['product_detailsModule'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_detailsModule'],
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_content_product', 'getDetailsModules'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_content'],
	'eval'                    => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'                     => "int(10) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['product_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_template'],
	'default'                 => 'product_latest',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_content_product', 'getProductTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['product_format'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_format'],
	'default'                 => 'product_month',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('product_day', 'product_month', 'product_year'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_content'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['product_order'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_order'],
	'default'                 => 'descending',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options'                 => array('ascending', 'descending'),
	'reference'               => &$GLOBALS['TL_LANG']['MSC'],
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['product_showQuantity'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_content']['product_showQuantity'],
	'exclude'                 => true,
	'inputType'               => 'checkbox',
	'sql'                     => "char(1) NOT NULL default ''"
);


/**
 * Provide miscellaneous methods that are used by the data configuration array.
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class tl_content_product extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	 * Check permissions to edit table tl_content
	 */
	public function checkPermission()
	{
		if ($this->User->isAdmin)
		{
			return;
		}

		// Set the root IDs
		if (!is_array($this->User->product) || empty($this->User->product))
		{
			$root = array(0);
		}
		else
		{
			$root = $this->User->product;
		}

		// Check the current action
		switch (Input::get('act'))
		{
			case 'paste':
				// Allow
				break;

			case '': // empty
			case 'create':
			case 'select':
				// Check access to the product item
				if (!$this->checkAccessToElement(CURRENT_ID, $root, true))
				{
					$this->redirect('contao/main.php?act=error');
				}
				break;

			case 'editAll':
			case 'deleteAll':
			case 'overrideAll':
			case 'cutAll':
			case 'copyAll':
				// Check access to the parent element if a content element is moved
				if ((Input::get('act') == 'cutAll' || Input::get('act') == 'copyAll') && !$this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2)))
				{
					$this->redirect('contao/main.php?act=error');
				}

				$objCes = $this->Database->prepare("SELECT id FROM tl_content WHERE ptable='tl_product' AND pid=?")
										 ->execute(CURRENT_ID);

				$session = $this->Session->getData();
				$session['CURRENT']['IDS'] = array_intersect($session['CURRENT']['IDS'], $objCes->fetchEach('id'));
				$this->Session->setData($session);
				break;

			case 'cut':
			case 'copy':
				// Check access to the parent element if a content element is moved
				if (!$this->checkAccessToElement(Input::get('pid'), $root, (Input::get('mode') == 2)))
				{
					$this->redirect('contao/main.php?act=error');
				}
				// NO BREAK STATEMENT HERE

			default:
				// Check access to the content element
				if (!$this->checkAccessToElement(Input::get('id'), $root))
				{
					$this->redirect('contao/main.php?act=error');
				}
				break;
		}
	}


	/**
	 * Check access to a particular content element
	 * @param integer
	 * @param array
	 * @param boolean
	 * @return boolean
	 */
	protected function checkAccessToElement($id, $root, $blnIsPid=false)
	{
		if ($blnIsPid)
		{
			$objArchive = $this->Database->prepare("SELECT a.id, n.id AS nid FROM tl_product n, tl_product_archive a WHERE n.id=? AND n.pid=a.id")
										 ->limit(1)
										 ->execute($id);
		}
		else
		{
			$objArchive = $this->Database->prepare("SELECT a.id, n.id AS nid FROM tl_content c, tl_product n, tl_product_archive a WHERE c.id=? AND c.pid=n.id AND n.pid=a.id")
										 ->limit(1)
										 ->execute($id);
		}

		// Invalid ID
		if ($objArchive->numRows < 1)
		{
			$this->log('Invalid product content element ID ' . $id, __METHOD__, TL_ERROR);
			return false;
		}

		// The product archive is not mounted
		if (!in_array($objArchive->id, $root))
		{
			$this->log('Not enough permissions to modify article ID ' . $objArchive->nid . ' in product archive ID ' . $objArchive->id, __METHOD__, TL_ERROR);
			return false;
		}

		return true;
	}

	/**
	 * Get all product archives and return them as array
	 * @return array
	 */// TODO: integrate es tree possible?
	public function getProductArchives()
	{
		if (!$this->User->isAdmin && !is_array($this->User->product))
		{
			return array();
		}

		$arrArchives = array();
		$objArchives = $this->Database->execute("SELECT id, title FROM tl_product_archive ORDER BY title");

		while ($objArchives->next())
		{
			if ($this->User->hasAccess($objArchives->id, 'product'))
			{
				$arrArchives[$objArchives->id] = $objArchives->title;
			}
		}

		return $arrArchives;
	}


	/**
	 * Get all product details modules and return them as array
	 * @return array
	 */
	public function getDetailsModules()
	{
		$arrModules = array();
		$objModules = $this->Database->execute("SELECT m.id, m.name, t.name AS theme FROM tl_module m LEFT JOIN tl_theme t ON m.pid=t.id WHERE m.type='productdetails' ORDER BY t.name, m.name");

		while ($objModules->next())
		{
			$arrModules[$objModules->theme][$objModules->id] = $objModules->name . ' (ID ' . $objModules->id . ')';
		}

		return $arrModules;
	}


	/**
	 * Return all product templates as array
	 * @return array
	 */
	public function getproductTemplates()
	{
		return $this->getTemplateGroup('product_');
	}

}
