<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */


/**
 * Namespaces
 */
/*
ClassLoader::addNamespaces(array
(
    'Contao'
));
*/ 

/**
 * Back end modules
 */
$GLOBALS['BE_MOD']['content']['product'] = array
(
		'tables'      => array('tl_product_archive', 'tl_product', 'tl_content'),
		'icon'        => 'system/modules/zixProducts/assets/icon.png',
		'table'       => array('TableWizard', 'importTable'),
		'list'        => array('ListWizard', 'importList')
);


/**
 * Front end modules
 */
$GLOBALS['FE_MOD']['product'] = array
(	
		'productlist'     => 'ModuleProductList',
		'productdetails'  => 'ModuleProductDetails'
		//'productarchive' => 'ModuleProductArchive',
		//'productmenu'    => 'ModuleProductMenu' // might be useful
);

/**
 * Content elements
 */
$GLOBALS['TL_CTE']['product'] = array
(
		'productlist'		=> 'ContentProductList',
		'productdetails'	=> 'ContentProductDetails'
);


/**
 * Cron jobs
 */
//$GLOBALS['TL_CRON']['daily'][] = array('News', 'generateFeeds');


/**
 * Register hook to add news items to the indexer
 */
$GLOBALS['TL_HOOKS']['getSearchablePages'][] = array('Product', 'getSearchablePages');


/**
 * Add permissions
 */
// ???
$GLOBALS['TL_PERMISSIONS'][] = 'product';
$GLOBALS['TL_PERMISSIONS'][] = 'productp';
