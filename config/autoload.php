<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */

/**
 * Register namespaces
 */
ClassLoader::addNamespaces(array
(
	'IXTENSA'
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\Product'              => 'system/modules/zixProducts/classes/Product.php',

	// Models
	'IXTENSA\ProductArchiveModel'  => 'system/modules/zixProducts/models/ProductArchiveModel.php',
	'IXTENSA\ProductModel'         => 'system/modules/zixProducts/models/ProductModel.php',

	// Modules
	'IXTENSA\ModuleProduct'        => 'system/modules/zixProducts/modules/ModuleProduct.php',	
	'IXTENSA\ModuleProductList'    => 'system/modules/zixProducts/modules/ModuleProductList.php',
	'IXTENSA\ModuleProductDetails' => 'system/modules/zixProducts/modules/ModuleProductDetails.php',
	//'IXTENSA\ModuleProductMenu'    => 'system/modules/zixProducts/modules/ModuleProductMenu.php',	
	//'IXTENSA\ModuleProductArchive' => 'system/modules/zixProducts/modules/ModuleProductArchive.php'

	// Content elements
	'IXTENSA\ContentProduct'        => 'system/modules/zixProducts/elements/ContentProduct.php',
	'IXTENSA\ContentProductList'    => 'system/modules/zixProducts/elements/ContentProductList.php',
	'IXTENSA\ContentProductDetails' => 'system/modules/zixProducts/elements/ContentProductDetails.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'mod_productlist'      => 'system/modules/zixProducts/templates/modules',
	'mod_productdetails'   => 'system/modules/zixProducts/templates/modules',
	//'mod_productarchive'   => 'system/modules/zixProducts/templates/modules',
	//'mod_productmenu'      => 'system/modules/zixProducts/templates/modules', // menu module could be considered useful
	//'mod_productmenu_day'  => 'system/modules/zixProducts/templates/modules',
	//'mod_productmenu_year' => 'system/modules/zixProducts/templates/modules',
	'product_full'         => 'system/modules/zixProducts/templates/product',
	'product_teaser'       => 'system/modules/zixProducts/templates/product',
	'product_short'        => 'system/modules/zixProducts/templates/product',
));
