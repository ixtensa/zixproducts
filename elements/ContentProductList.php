<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Leo Feyer
 *
 * @license LGPL-3.0+
 */

namespace IXTENSA;


/**
 * Content element "product list".
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class ContentProductList extends \ContentProduct
{

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_productlist';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');
			$objTemplate->wildcard = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['productlist'][0]) . ' ###';

			$objProductArchives = \ProductArchiveModel::findMultipleByIds(deserialize($this->product_archives));
			
			if($objProductArchives !== null)
			{
				$objTemplate->wildcard .= '<br>';
				while ($objProductArchives->next()) 
				{
					$objTemplate->wildcard .= $objProductArchives->title.' (ID: '.$objProductArchives->id.')<br>';
				}
			}

			return $objTemplate->parse();
		}

		$this->product_archives = $this->sortOutProtected(deserialize($this->product_archives));

		// Return if there are no archives
		if (!is_array($this->product_archives) || empty($this->product_archives))
		{
			return '';
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
		$offset = intval($this->skipFirst);
		$limit = null;

		// Maximum number of items
		if ($this->numberOfItems > 0)
		{
			$limit = $this->numberOfItems;
		}

		// Handle featured product
		if ($this->product_featured == 'featured')
		{
			$blnFeatured = true;
		}
		elseif ($this->product_featured == 'unfeatured')
		{
			$blnFeatured = false;
		}
		else
		{
			$blnFeatured = null;
		}

		$this->Template->articles = array();
		$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];

		// Get the total number of items
		$intTotal = \ProductModel::countPublishedByPids($this->product_archives, $blnFeatured);

		if ($intTotal < 1)
		{
			return;
		}

		$total = $intTotal - $offset;

		// Split the results
		if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
		{
			// Adjust the overall limit
			if (isset($limit))
			{
				$total = min($limit, $total);
			}

			// Get the current page
			$id = 'page_n' . $this->id;
			$page = \Input::get($id) ?: 1;

			// Do not index or cache the page if the page number is outside the range
			if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
			{
				global $objPage;
				$objPage->noSearch = 1;
				$objPage->cache = 0;

				// Send a 404 header
				header('HTTP/1.1 404 Not Found');
				return;
			}

			// Set limit and offset
			$limit = $this->perPage;
			$offset += (max($page, 1) - 1) * $this->perPage;
			$skip = intval($this->skipFirst);

			// Overall limit
			if ($offset + $limit > $total + $skip)
			{
				$limit = $total + $skip - $offset;
			}

			// Add the pagination menu
			$objPagination = new \Pagination($total, $this->perPage, \Config::get('maxPaginationLinks'), $id);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}

		// Get the items
		if (isset($limit))
		{
			$objArticles = \ProductModel::findPublishedByPids($this->product_archives, $blnFeatured, $limit, $offset);
			//$objArticles = \ProductModel::findPublishedByParentAndIdOrAlias(\Input::get('items'), $this->product_archives);
		}
		else
		{
			$objArticles = \ProductModel::findPublishedByPids($this->product_archives, $blnFeatured, 0, $offset);
			//$objArticles = \ProductModel::findPublishedByParentAndIdOrAlias(\Input::get('items'), $this->product_archives);
		}

		// Add the articles
		if ($objArticles !== null)
		{
			$this->Template->articles = $this->parseArticles($objArticles);
		}

		$this->Template->archives = $this->product_archives;
	}
}
