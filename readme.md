# zixProducts

* Copyright: Ixtensa (http://wwww.ixtensa.de)
* Author: Fabian Perrey (f.perrey@gmx.de)
* License: LGPL


### System requirements
* PHP 5.4.0+
* Contao 3.5
* Widget tree picker 2.0.1 ()


### Description
zixProducts is an archive based product management for Contao open source CMS. It's built based on the Contao core news and event mocules and intends to offer the same workflow as these modules. So it basically works the same way as the core archive modules but uses its own table and data fields. It provides a tree view based archive management and archives and product elements inside are manually sorted.
In addtion to the frontend modules this tool comes with content elements for product-lists and product-details.

**Widget tree picker**
This module supports the Widget tree picker for Contao created by codefog (https://github.com/codefog/contao-widget_tree_picker). With this tool your product archvies can be picked within a tree-view just like it works for the Contao file- and pagePicker. Without this tool a simple list will generated (same as for news/event).
(Note: right now this tool is an requirement, not having it installed will end up with errors. A fallback will be implemented next)


### Getting Started
After installing you will find the archive "products" within your content menu options. Inside there you can create new archives as you would do it for the Contao news or event modules. Archvies can be manually sorted and nested the same way it works for the site structure.
Within every archive the products themselves are created. Products work the same way as news entries do. They have meta information (title, teaser, image, etc.) and a content area where you are able to freely create Contao contenet elements.
To integrate your products into the frontend you can use the frontend modules Productlist and Product details. There your can configurate the output and recursively pick your archives. If you have the the widget tree picker extension installed you can conveniently pick from a tree view instead of a simple list. To increase flexibility this tool also provides content elements for product-lists and details. The are configurated completely like the frontend moduels. For outputting your products three templates are included (full, short, teaser).


### Feature plans
* sorting for output modules
* implement additional output modules (archive, menu, etc.)
* translation improvements
* code cleaning/optimization